/**********************************************************************
 myAVR MK2 example: LCD - example program for the Dot Matrix LCD
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#include "def.h"
#include "lcd.h"


// Prototypes
void init();
void led_signal(uint8_t);


void init()
{
	// Init LEDs
	DDR_LED  |=   (1<<LED0) | (1<<LED1) | (1<<LED2);  // set output
	PORT_LED &= ~((1<<LED0) | (1<<LED1) | (1<<LED2)); // set LEDs off

	// Init switches
	DDR_SW  &= ~((1<<SW0) | (1<<SW1)); // set input
	PORT_SW |=   (1<<SW0) | (1<<SW1);  // set pullup resistors (low active switches)

	// Init LCD
	lcd_init();

	return;
}


void led_signal(uint8_t s)
{
	switch (s) {
		case 0:
			PORT_LED &= ~(1<<LED0); // LED.0 off
			PORT_LED &= ~(1<<LED1); // LED.1 off
			PORT_LED &= ~(1<<LED2); // LED.2 off
			break;
		case 1:
			PORT_LED |=  (1<<LED0); // LED.0 on
			PORT_LED &= ~(1<<LED1); // LED.1 off
			PORT_LED &= ~(1<<LED2); // LED.2 off
			break;
		case 2:
			PORT_LED &= ~(1<<LED0); // LED.0 off
			PORT_LED |=  (1<<LED1); // LED.1 on
			PORT_LED &= ~(1<<LED2); // LED.2 off
			break;
		case 3:
			PORT_LED |=  (1<<LED0); // LED.0 on
			PORT_LED |=  (1<<LED1); // LED.1 on
			PORT_LED &= ~(1<<LED2); // LED.2 off
			break;
		case 4:
			PORT_LED &= ~(1<<LED0); // LED.0 off
			PORT_LED &= ~(1<<LED1); // LED.1 off
			PORT_LED |=  (1<<LED2); // LED.2 on
			break;
		case 5:
			PORT_LED |=  (1<<LED0); // LED.0 on
			PORT_LED &= ~(1<<LED1); // LED.1 off
			PORT_LED |=  (1<<LED2); // LED.2 on
			break;
		case 6:
			PORT_LED &= ~(1<<LED0); // LED.0 off
			PORT_LED |=  (1<<LED1); // LED.1 on
			PORT_LED |=  (1<<LED2); // LED.2 on
			break;
		case 7:
			PORT_LED |=  (1<<LED0); // LED.0 on
			PORT_LED |=  (1<<LED1); // LED.1 on
			PORT_LED |=  (1<<LED2); // LED.2 on
			break;
		default:
			PORT_LED &= ~(1<<LED0); // LED.0 off
			PORT_LED &= ~(1<<LED1); // LED.1 off
			PORT_LED &= ~(1<<LED2); // LED.2 off
		break;
	}

	return;
}


int main()
{
	uint8_t pin_sw = 0x00;

	init();

//	_delay_ms(50);
	lcd_pos(0, 0);
	lcd_text("Hello World!");
	lcd_pos(1, 4);
	lcd_text("Hello Mouse!");

	while (1) {
		pin_sw = PIN_SW;

		if (pin_sw & (1<<SW0)) {
			if (pin_sw & (1<<SW1)) {
				led_signal(1);
			}
			else {
				led_signal(5);
			}
		}
		else {
			if (pin_sw & (1<<SW1)) {
				led_signal(3);
			}
			else {
				led_signal(7);
			}
		}
	}

	return 0;
}

