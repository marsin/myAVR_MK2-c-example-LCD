#!/bin/bash
# myAVR MK2 example: LCD - example program for the Dot Matrix LCD
# Copyright (c) 2016 Martin Singer <martin.singer@web.de>

avr-gcc lcd.c main.c -mmcu=atmega8 -O1 -Wall -o fw.elf && \
	avr-objcopy -O ihex fw.elf fw.hex && \
	avrdude -p m8 -c avr911 -P /dev/ttyUSB0 -U flash:w:fw.hex:i

