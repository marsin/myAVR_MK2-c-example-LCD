/**********************************************************************
 myAVR MK2 example: LCD - example program for the Dot Matrix LCD
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

/**********************************************************************
 * Components:
   - myAVR MK2 board
   - myAVR LCD V2.5 Add-On board
   - mySmartUSB MK2 programmer
 (www.myavr.com)


 * Dot Matrix LCD:
   - 2 lines, 16 columns, a 5x7 pixel

 * Controller:
   AVR ATmega8L, 3.6864 MHz

 * Function:
   - Writes example text on LCD
   - Polls switches which turn LEDs on and off

 * Circuit:
   PortC.0 := Switch 1
   PortC.1 := Switch 2

   PortC.3 := LED 1 (green)
   PortC.4 := LED 2 (yellow)
   PortC.5 := LED 3 (red)

   PortB.0 := LCD R/W (read/write), JP R/W
   PortB.1 := LCD PWM (backlight),  JP PWM
   PortD.2 := LCD RS  (reset)
   PortD.3 := LCD E   (enable)
   PortD.4 := LCD DB4 (databit 4 of LCD)
   PortD.5 := LCD DB5 (databit 5 of LCD)
   PortD.6 := LCD DB6 (databit 6 of LCD)
   PortD.7 := LCD DB7 (databit 7 of LCD)
 **********************************************************************/

#define F_CPU 3686400ul

#include <avr/io.h>
#include <util/delay.h>

// Define: LEDs ports
#define PORT_LED PORTC
#define DDR_LED  DDRC
#define LED0     PC3
#define LED1     PC4
#define LED2     PC5

// Define: Switches ports
#define PORT_SW PORTC
#define DDR_SW  DDRC
#define PIN_SW  PINC
#define SW0     PC0
#define SW1     PC1

// Define: LCD ports
#define PORT_LCD PORTD
#define DDR_LCD  DDRD
#define LCD_RS   PD2
#define LCD_E    PD3
#define LCD_D4   PD4
#define LCD_D5   PD5
#define LCD_D6   PD6
#define LCD_D7   PD7

