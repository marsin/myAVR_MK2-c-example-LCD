/**********************************************************************
 myAVR MK2 example: LCD - example program for the Dot Matrix LCD
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#include "lcd.h"


// Prototypes
static void lcd_nibble(uint8_t);
static void lcd_byte(uint8_t);


/** Initialize the LCD
 * Setup the LCD mode:
 * 4 bit mode, 2 lines, 2x7 pixel, on, no cursor, no blink
 */
void lcd_init()
{
	DDR_LCD  |= (1<<LCD_RS) | (1<<LCD_E) | (1<<LCD_D4) | (1<<LCD_D5) | (1<<LCD_D6) | (1<<LCD_D7); // set output
	PORT_LCD |= (1<<LCD_RS) | (1<<LCD_E) | (1<<LCD_D4) | (1<<LCD_D5) | (1<<LCD_D6) | (1<<LCD_D7);

	PORT_LCD &= ~((1<<LCD_E) | (1<<LCD_RS)); // command
	_delay_ms(15);                           // wait 15ms

	lcd_nibble(0x30);
	_delay_ms(5);                            // wait > 4.1ms

	lcd_nibble(0x30);
	_delay_us(100);                          // wait 100us

	lcd_nibble(0x30);                        // 8bit mode
	_delay_us(100);                          // wait 100us

	lcd_nibble(0x20);                        // 4bit mode
	_delay_us(100);                          // wait 100us

	lcd_command(0x28);                       // 2lines, 2x7px
	lcd_command(0x08);                       // display off
	lcd_command(0x01);                       // display clear
	lcd_command(0x06);                       // cursor increment
	lcd_command(0x0C);                       // on, no cursor, no blink

	return;
}


/** LCD send nibble
 * Writes a half byte (nibble) into the definded LCD port register bits.
 * Then it sends a signal for the LCD to read the nibble.
 * The valid four bits of the nibble have to be on position D7..4.
 * @param[in] n the byte with the nibble on D7..4
 */
static void lcd_nibble(uint8_t n)
{
	PORT_LCD &= ~((1<<LCD_D7) | (1<<LCD_D6) | (1<<LCD_D5) | (1<<LCD_D4));

	if (n & (1<<7)) PORT_LCD |= (1<<LCD_D7);
	if (n & (1<<6)) PORT_LCD |= (1<<LCD_D6);
	if (n & (1<<5)) PORT_LCD |= (1<<LCD_D5);
	if (n & (1<<4)) PORT_LCD |= (1<<LCD_D4);

	PORT_LCD |= (1<<LCD_E);
	_delay_us(1);
	PORT_LCD &= ~(1<<LCD_E);

	return;
}


/** LCD send byte
 * Writes a data byte into the LCD port registers, using the function lcd_nibble()
 * It transfers the data bits D7..4 first, then the data bits D3..0
 * @param[in] b the data byte
 * @see lcd_nibble()
 */
static void lcd_byte(uint8_t b)
{
	lcd_nibble(b);
	lcd_nibble(b<<4);

	_delay_us(45);
	return;
}


/** LCD send command
 * sends command byte to the LCD, using the function lcd_byte()
 * @param[in] c the command byte
 * @see lcd_byte()
 */
void lcd_command(uint8_t c)
{
	PORT_LCD &= ~(1<<LCD_RS);
	lcd_byte(c);

	switch (c) {
		case 0:
		case 1:
		case 2:
			_delay_ms(2);
			break;
		default:
			break;
	}

	return;
}


/** LCD send data
 * sends data byte to the LCD, using the function lcd_byte()
 * @param[in] d the data byte
 * @see lcd_byte()
 */
void lcd_data(uint8_t d)
{
	PORT_LCD |= (1<<LCD_RS);
	lcd_byte(d);

	return;
}


/** LCD Text
 * write ASCII text signs on the LCD, starting from the cursor position
 * @param[in] *t pointer on a character array
 */
void lcd_text(char *t)
{
	while (*t) {
		lcd_data(*t);
		++t;
	}

	return;
}

/** LCD Position
 * set the position of the LCD cursor
 * @param[in] l the line number
 * @param[in] c the column number
 */
void lcd_pos( uint8_t l, uint8_t c)
{
	while (l--) {
		c += 0x40;
	}

	lcd_command(0x80 + c);
	return;
}

